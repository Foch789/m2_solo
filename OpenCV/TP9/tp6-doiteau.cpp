/*
    Exemples de transformations en OpenCV, avec zoom, seuil et affichage en
    couleurs. L'image de niveau est en CV_32SC1.

    $ make ex01-transfos
    $ ./ex01-transfos [-mag width height] [-thr seuil] image_in [image_out]

    CC BY-SA Edouard.Thiel@univ-amu.fr - 07/09/2020

                        --------------------------------

    Renommez ce fichier tp<n°>-<vos-noms>.cpp 
    Écrivez ci-dessous vos NOMS Prénoms et la date de la version :

    DOITEAU Laurent - version du 02/11/2020
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <opencv2/opencv.hpp>
#include "gd-util.hpp"

#include <vector>
#include <string>

//----------------------------------- M Y -------------------------------------

enum NumeroMasque
{
    M_D4,
    M_D8,
    M_2_3,
    M_3_4,
    M_5_7_11,
    M_LAST
};

struct Ponderation
{
    int x, y, w;

    Ponderation() : x(0), y(0), w(0)
    {
    }

    Ponderation(int _x, int _y, int _w) : x(_x), y(_y), w(_w)
    {
    }
};

struct DemiMasque
{
    std::vector<Ponderation> listPonderation;
    NumeroMasque numero;
    std::string nameMasque;

    DemiMasque(NumeroMasque _numeroMasque) : listPonderation{}, numero{_numeroMasque}
    {
        switch (_numeroMasque)
        {
        case NumeroMasque::M_D4:
            listPonderation.push_back(Ponderation{1, 0, 1});
            listPonderation.push_back(Ponderation{0, 1, 1});
            nameMasque = "Masque_M_D4";
            break;
        case NumeroMasque::M_D8:
            listPonderation.push_back(Ponderation{1, 0, 1});
            listPonderation.push_back(Ponderation{1, 1, 1});
            listPonderation.push_back(Ponderation{0, 1, 1});
            listPonderation.push_back(Ponderation{-1, 1, 1});
            nameMasque = "Masque_M_D8";
            break;
        case NumeroMasque::M_2_3:
            listPonderation.push_back(Ponderation{1, 0, 2});
            listPonderation.push_back(Ponderation{1, 1, 3});
            listPonderation.push_back(Ponderation{0, 1, 2});
            listPonderation.push_back(Ponderation{-1, 1, 3});
            nameMasque = "Masque_M_2_3";
            break;
        case NumeroMasque::M_3_4:
            listPonderation.push_back(Ponderation{1, 0, 3});
            listPonderation.push_back(Ponderation{1, 1, 4});
            listPonderation.push_back(Ponderation{0, 1, 3});
            listPonderation.push_back(Ponderation{-1, 1, 4});
            nameMasque = "Masque_M_3_4";
            break;
        case NumeroMasque::M_5_7_11:
            listPonderation.push_back(Ponderation{1, 0, 5});
            listPonderation.push_back(Ponderation{1, 1, 7});
            listPonderation.push_back(Ponderation{-1, 1, 7});
            listPonderation.push_back(Ponderation{2, 1, 11});
            listPonderation.push_back(Ponderation{1, 2, 11});
            listPonderation.push_back(Ponderation{-1, 2, 11});
            listPonderation.push_back(Ponderation{-2, 1, 11});
            nameMasque = "Masque_5_7_11";
            break;
        default:
            break;
        }
    }
};

int FILTRE = 0;

class My
{
public:
    cv::Mat img_src, img_res1, img_res2, img_niv, img_coul;
    Loupe loupe;
    int seuil = 127;
    int clic_x = 0;
    int clic_y = 0;
    int clic_n = 0;

    enum Recalc
    {
        R_RIEN,
        R_LOUPE,
        R_TRANSFOS,
        R_SEUIL
    };
    Recalc recalc = R_SEUIL;

    void reset_recalc() { recalc = R_RIEN; }
    void set_recalc(Recalc level)
    {
        if (level > recalc)
            recalc = level;
    }
    int need_recalc(Recalc level) { return level <= recalc; }

    // Rajoutez ici des codes A_TRANSx pour le calcul et l'affichage
    enum Affi
    {
        A_ORIG,
        A_SEUIL,
        A_TRANS1,
        A_TRANS2,
        A_TRANS3,
        A_TRANS4,
        A_TRANS5,
        A_TRANS6
    };
    Affi affi = A_ORIG;

    NumeroMasque numero_masque = NumeroMasque::M_D4;
    DemiMasque demiMasque_courant = DemiMasque{NumeroMasque::M_D4};
};

//----------------------- T R A N S F O R M A T I O N S -----------------------

// Placez ici vos fonctions de transformations à la place de ces exemples

bool inPicture(cv::Mat img_niv, int x, int y)
{
    return 0 <= y && y < img_niv.rows && 0 <= x && x < img_niv.cols;
}

void calculer_Rosenfeld_DT(cv::Mat img_niv, DemiMasque demi_masque)
{
    for (int y = 0; y < img_niv.rows; y++)
        for (int x = 0; x < img_niv.cols; x++)
        {
            int &value = img_niv.at<int>(y, x);
            if (value != 0)
            {
                int result = img_niv.at<int>(y, x);
                for (size_t i = 0; i < demi_masque.listPonderation.size(); i++)
                {
                    int tempX = x - demi_masque.listPonderation[i].x;
                    int tempY = y - demi_masque.listPonderation[i].y;
                    if (inPicture(img_niv, tempX, tempY))
                    {
                        int resultTemp = (img_niv.at<int>(tempY, tempX) + demi_masque.listPonderation[i].w);
                        if (result > resultTemp)
                            result = resultTemp;
                    }
                }
                value = result;
            }
        }

    for (int y = img_niv.rows - 1; y >= 0; y--)
        for (int x = img_niv.cols - 1; x >= 0; x--)
        {
            int &value = img_niv.at<int>(y, x);
            if (value != 0)
            {
                int result = img_niv.at<int>(y, x);
                for (size_t i = 0; i < demi_masque.listPonderation.size(); i++)
                {
                    int tempX = x + demi_masque.listPonderation[i].x;
                    int tempY = y + demi_masque.listPonderation[i].y;
                    if (inPicture(img_niv, tempX, tempY))
                    {
                        int resultTemp = (img_niv.at<int>(tempY, tempX) + demi_masque.listPonderation[i].w);
                        if (result > resultTemp)
                            result = resultTemp;
                    }
                }
                value = result;
            }
        }
}

void detecter_maximums_locaux(cv::Mat img_niv, DemiMasque demi_masque)
{
    cv::Mat img_temp = img_niv.clone();

    auto max = [&](int x, int y) {
        for (const auto &p : demi_masque.listPonderation)
            if ((inPicture(img_temp, x + p.x, y + p.y) && img_temp.at<int>(y, x) <= img_temp.at<int>(y + p.y, x + p.x) - p.w) ||
                (inPicture(img_temp, x - p.x, y - p.y) && img_temp.at<int>(y, x) <= img_temp.at<int>(y - p.y, x - p.x) - p.w))
                return false;

        return true;
    };

    for (int y = 0; y < img_temp.rows; y++)
        for (int x = 0; x < img_temp.cols; x++)
            if (img_temp.at<int>(y, x) != 0 && !max(x, y))
                img_niv.at<int>(y, x) = 0;
}

void calculer_Rosenfeld_RDT(cv::Mat img_niv, DemiMasque demi_masque)
{
    for (int y = 0; y < img_niv.rows; y++)
        for (int x = 0; x < img_niv.cols; x++)
        {
            int &value = img_niv.at<int>(y, x);
            int result = img_niv.at<int>(y, x);
            for (size_t i = 0; i < demi_masque.listPonderation.size(); i++)
            {
                int tempX = x - demi_masque.listPonderation[i].x;
                int tempY = y - demi_masque.listPonderation[i].y;
                if (inPicture(img_niv, tempX, tempY))
                {
                    int resultTemp = (img_niv.at<int>(tempY, tempX) - demi_masque.listPonderation[i].w);
                    if (result < resultTemp)
                        result = resultTemp;
                }
            }
            value = result;
        }

    for (int y = img_niv.rows - 1; y >= 0; y--)
        for (int x = img_niv.cols - 1; x >= 0; x--)
        {
            int &value = img_niv.at<int>(y, x);
            int result = img_niv.at<int>(y, x);
            for (size_t i = 0; i < demi_masque.listPonderation.size(); i++)
            {
                int tempX = x + demi_masque.listPonderation[i].x;
                int tempY = y + demi_masque.listPonderation[i].y;
                if (inPicture(img_niv, tempX, tempY))
                {
                    int resultTemp = (img_niv.at<int>(tempY, tempX) - demi_masque.listPonderation[i].w);
                    if (result < resultTemp)
                        result = resultTemp;
                }
            }
            value = result;
        }
}

void filtrer_formes_avec_maximums_locaux(cv::Mat img_niv, DemiMasque masque, int filtre) {
    
    cv::Mat img_copy = img_niv.clone();

    calculer_Rosenfeld_DT(img_copy, masque);
    detecter_maximums_locaux(img_copy, masque);

    for (int y = 0; y < img_copy.rows; y++)
        for (int x = 0; x < img_copy.cols; x++)
        {
            int &pixel = img_copy.at<int>(y,x);
            if (pixel <= filtre) {
                pixel = 0;
            }
        }

    calculer_Rosenfeld_RDT(img_copy, masque);

    for (int y = 0; y < img_niv.rows; y++)
        for (int x = 0; x < img_niv.cols; x++)
        {
            int &pixel = img_niv.at<int>(y,x);
            int &pixel_clone = img_copy.at<int>(y,x);
            if (pixel_clone > filtre)
                pixel = 11;
            else if (pixel_clone > 0) 
                pixel = 8;
            else if (pixel_clone == 0 && pixel > 0)
                pixel = 12; 
        }

}

void calculer_sedt_saito_toriwaki (cv::Mat img_niv) {
    
    for (int y = 0; y < img_niv.rows; y++)
    {
        int dist = 0;
        for (int x = 0; x < img_niv.cols; x++)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) dist = 0;
            else pix = std::pow(++dist, 2);
        }
        dist = 0;
        for (int x = img_niv.cols - 1; x >= 0; x--)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) dist = 0;
            else pix = std::min<int>(std::pow(++dist, 2), pix);
        }
    }

    for(int x = 0; x < img_niv.cols; x++)
    {
        std::vector<int> col_copy;
        for(int y = 0; y < img_niv.rows; y++)
        {
            col_copy.push_back(img_niv.at<int>(y, x));
        }
        for(int y = 0; y < img_niv.rows; y++)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) continue;
            int min_val = pix;
            for(int y2 = y-1; y2 >= 0; y2--)
            {
                int dist = std::pow(y - y2, 2);
                if(dist >= min_val) break;
                min_val = std::min<int>(col_copy[y2] + dist, min_val);
            }
            min_val = std::min<int>(std::pow(y+1, 2), min_val);
            for(int y2 = y+1; y2 < img_niv.rows; y2++)
            {
                int dist = std::pow(y - y2, 2);
                if(dist >= min_val) break;
                min_val = std::min<int>(col_copy[y2] + dist, min_val);
            }
            min_val = std::min<int>(std::pow(y - img_niv.rows, 2), min_val);
            pix = min_val;
        }
    }

}

void calculer_sedt_courbes_niveau (cv::Mat img_niv)
{
    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int& pix = img_niv.at<int>(y, x);
        pix = std::sqrt(pix);
    }
}


// Appelez ici vos transformations selon affi
void effectuer_transformations(My::Affi affi, cv::Mat img_niv, DemiMasque masque)
{
    switch (affi)
    {
    case My::A_TRANS1:
        calculer_Rosenfeld_DT(img_niv, masque);
        break;
    case My::A_TRANS2:
        calculer_Rosenfeld_DT(img_niv, masque);
        detecter_maximums_locaux(img_niv, masque);
        break;
    case My::A_TRANS3:
        calculer_Rosenfeld_DT(img_niv, masque);
        detecter_maximums_locaux(img_niv, masque);
        calculer_Rosenfeld_RDT(img_niv, masque);
        break;
    case My::A_TRANS4:
        filtrer_formes_avec_maximums_locaux(img_niv, masque, FILTRE);
        break;
    case My::A_TRANS5:
        calculer_sedt_saito_toriwaki(img_niv);
        break;
    case My::A_TRANS6:
        calculer_sedt_saito_toriwaki(img_niv);
        calculer_sedt_courbes_niveau(img_niv)
        break;
    default:;
    }
}

//---------------------------- C A L L B A C K S ------------------------------

// Callback des sliders
void onZoomSlide(int pos, void *data)
{
    My *my = (My *)data;
    my->loupe.reborner(my->img_res1, my->img_res2);
    my->set_recalc(My::R_LOUPE);
}

void onSeuilSlide(int pos, void *data)
{
    My *my = (My *)data;
    my->set_recalc(My::R_SEUIL);
}

// Callback pour la souris
void onMouseEventSrc(int event, int x, int y, int flags, void *data)
{
    My *my = (My *)data;

    switch (event)
    {
    case cv::EVENT_LBUTTONDOWN:
        my->clic_x = x;
        my->clic_y = y;
        my->clic_n = 1;
        break;
    case cv::EVENT_MOUSEMOVE:
        // std::cout << "mouse move " << x << "," << y << std::endl;
        if (my->clic_n == 1)
        {
            my->loupe.deplacer(my->img_res1, my->img_res2,
                               x - my->clic_x, y - my->clic_y);
            my->clic_x = x;
            my->clic_y = y;
            my->set_recalc(My::R_LOUPE);
        }
        break;
    case cv::EVENT_LBUTTONUP:
        my->clic_n = 0;
        break;
    }
}

void onMouseEventLoupe(int event, int x, int y, int flags, void *data)
{
    My *my = (My *)data;

    switch (event)
    {
    case cv::EVENT_LBUTTONDOWN:
        my->loupe.afficher_tableau_valeurs(my->img_niv, x, y, 5, 4);
        break;
    }
}

void afficher_aide()
{
    // Indiquez les transformations ici
    std::cout << "Touches du clavier:\n"
                 "   a    affiche cette aide\n"
                 " hHlL   change la taille de la loupe\n"
                 "   i    inverse les couleurs de src\n"
                 "   o    affiche l'image src originale\n"
                 "   s    affiche l'image src seuillée\n"
                 "   1    affiche la transformation 1\n"
                 "   2    affiche la transformation 2\n"
                 "   3    affiche la transformation 3\n"
                 "   4    affiche la transformation 4\n"
                 "   5    affiche la transformation 5\n"
                 "   6    affiche la transformation 6\n"
                 "  esc   quitte\n"
              << std::endl;
}

// Callback "maison" pour le clavier
int onKeyPressEvent(int key, void *data)
{
    My *my = (My *)data;

    if (key < 0)
        return 0; // aucune touche pressée
    key &= 255;   // pour comparer avec un char
    if (key == 27)
        return -1; // ESC pour quitter

    switch (key)
    {
    case 'a':
        afficher_aide();
        break;
    case 'h':
    case 'H':
    case 'l':
    case 'L':
    {
        std::cout << "Taille loupe" << std::endl;
        int h = my->img_res2.rows, w = my->img_res2.cols;
        if (key == 'h')
            h = h >= 200 + 100 ? h - 100 : 200;
        else if (key == 'H')
            h = h <= 2000 - 100 ? h + 100 : 2000;
        else if (key == 'l')
            w = w >= 200 + 100 ? w - 100 : 200;
        else if (key == 'L')
            w = w <= 2000 - 100 ? w + 100 : 2000;
        my->img_res2 = cv::Mat(h, w, CV_8UC3);
        my->loupe.reborner(my->img_res1, my->img_res2);
        my->set_recalc(My::R_LOUPE);
    }
    break;
    case 'i':
        std::cout << "Couleurs inversées" << std::endl;
        inverser_couleurs(my->img_src);
        my->set_recalc(My::R_SEUIL);
        break;
    case 'o':
        std::cout << "Image originale" << std::endl;
        my->affi = My::A_ORIG;
        my->set_recalc(My::R_TRANSFOS);
        break;
    case 's':
        std::cout << "Image seuillée" << std::endl;
        my->affi = My::A_SEUIL;
        my->set_recalc(My::R_SEUIL);
        break;
    case 'd':
        my->numero_masque = static_cast<NumeroMasque>(static_cast<int>(my->numero_masque) + 1);
        if (my->numero_masque == M_LAST)
            my->numero_masque = M_D4;

        my->demiMasque_courant = DemiMasque{my->numero_masque};
        std::cout << my->demiMasque_courant.nameMasque << std::endl;
        my->set_recalc(My::R_SEUIL);
        break;

    // Rajoutez ici des touches pour les transformations.
    // Dans my->set_recalc, passez :
    //   My::R_SEUIL pour faire le calcul à partir de l'image originale seuillée
    //   My::R_TRANSFOS pour faire le calcul à partir de l'image actuelle
    case '1':
        std::cout << "Transformation 1" << std::endl;
        my->affi = My::A_TRANS1;
        my->set_recalc(My::R_SEUIL);
        break;
    case '2':
        std::cout << "Transformation 2" << std::endl;
        my->affi = My::A_TRANS2;
        my->set_recalc(My::R_SEUIL);
        break;
    case '3':
        std::cout << "Transformation 3" << std::endl;
        my->affi = My::A_TRANS3;
        my->set_recalc(My::R_SEUIL);
        break;
    case '4':
        std::cout << "Transformation 4" << std::endl;
        my->affi = My::A_TRANS4;
        my->set_recalc(My::R_SEUIL);
        break;
    case '5':
        std::cout << "Transformation 5" << std::endl;
        my->affi = My::A_TRANS5;
        my->set_recalc(My::R_SEUIL);
        break;
    case '6':
        std::cout << "Transformation 6" << std::endl;
        my->affi = My::A_TRANS6;
        my->set_recalc(My::R_SEUIL);
        break;

    default:
        //std::cout << "Touche '" << char(key) << "'" << std::endl;
        break;
    }
    return 1;
}

//---------------------------------- M A I N ----------------------------------

void afficher_usage(char *nom_prog)
{
    std::cout << "Usage: " << nom_prog
              << "[-mag width height] [-thr seuil] in1 [out2]"
              << std::endl;
}

int main(int argc, char **argv)
{
    My my;
    char *nom_in1, *nom_out2, *nom_prog = argv[0];
    int zoom_w = 600, zoom_h = 500;

    while (argc - 1 > 0)
    {
        if (!strcmp(argv[1], "-mag"))
        {
            if (argc - 1 < 3)
            {
                afficher_usage(nom_prog);
                return 1;
            }
            zoom_w = atoi(argv[2]);
            zoom_h = atoi(argv[3]);
            argc -= 3;
            argv += 3;
        }
        else if (!strcmp(argv[1], "-thr"))
        {
            if (argc - 1 < 2)
            {
                afficher_usage(nom_prog);
                return 1;
            }
            my.seuil = atoi(argv[2]);
            argc -= 2;
            argv += 2;
        }
        else
            break;
    }
    if (argc - 1 < 1 or argc - 1 > 2)
    {
        afficher_usage(nom_prog);
        return 1;
    }
    nom_in1 = argv[1];
    nom_out2 = (argc - 1 == 2) ? argv[2] : NULL;

    // Lecture image
    my.img_src = cv::imread(nom_in1, cv::IMREAD_COLOR); // produit du 8UC3
    if (my.img_src.empty())
    {
        std::cout << "Erreur de lecture" << std::endl;
        return 1;
    }

    // Création résultats
    my.img_res1 = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.img_res2 = cv::Mat(zoom_h, zoom_w, CV_8UC3);
    my.img_niv = cv::Mat(my.img_src.rows, my.img_src.cols, CV_32SC1);
    my.img_coul = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.loupe.reborner(my.img_res1, my.img_res2);

    // Création fenêtre
    cv::namedWindow("ImageSrc", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar("Zoom", "ImageSrc", &my.loupe.zoom, my.loupe.zoom_max,
                       onZoomSlide, &my);
    cv::createTrackbar("Seuil", "ImageSrc", &my.seuil, 255,
                       onSeuilSlide, &my);
    cv::createTrackbar ("Filtre", "ImageSrc", &FILTRE, 255,
                        onSeuilSlide, &my);
    cv::setMouseCallback("ImageSrc", onMouseEventSrc, &my);

    cv::namedWindow("Loupe", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback("Loupe", onMouseEventLoupe, &my);

    afficher_aide();

    // Boucle d'événements
    for (;;)
    {

        if (my.need_recalc(My::R_SEUIL))
        {
            // std::cout << "Calcul seuil" << std::endl;
            cv::Mat img_gry;
            cv::cvtColor(my.img_src, img_gry, cv::COLOR_BGR2GRAY);
            cv::threshold(img_gry, img_gry, my.seuil, 255, cv::THRESH_BINARY);
            img_gry.convertTo(my.img_niv, CV_32SC1, 1., 0.);
        }

        if (my.need_recalc(My::R_TRANSFOS))
        {
            // std::cout << "Calcul transfos" << std::endl;
            if (my.affi != My::A_ORIG)
            {
                effectuer_transformations(my.affi, my.img_niv, my.demiMasque_courant);
                representer_en_couleurs_vga(my.img_niv, my.img_coul);
            }
            else
                my.img_coul = my.img_src.clone();
        }

        if (my.need_recalc(My::R_LOUPE))
        {
            // std::cout << "Calcul loupe puis affichage" << std::endl;
            my.loupe.dessiner_rect(my.img_coul, my.img_res1);
            my.loupe.dessiner_portion(my.img_coul, my.img_res2);
            cv::imshow("ImageSrc", my.img_res1);
            cv::imshow("Loupe", my.img_res2);
        }
        my.reset_recalc();

        // Attente du prochain événement sur toutes les fenêtres, avec un
        // timeout de 15ms pour détecter les changements de flags
        int key = cv::waitKey(15);

        // Gestion des événements clavier avec une callback "maison" que l'on
        // appelle nous-même. Les Callbacks souris et slider sont directement
        // appelées par waitKey lors de l'attente.
        if (onKeyPressEvent(key, &my) < 0)
            break;
    }

    // Enregistrement résultat
    if (nom_out2)
    {
        if (!cv::imwrite(nom_out2, my.img_coul))
            std::cout << "Erreur d'enregistrement" << std::endl;
        else
            std::cout << "Enregistrement effectué" << std::endl;
    }
    return 0;
}
