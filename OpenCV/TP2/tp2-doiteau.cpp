/*
    Exemples de transformations en OpenCV, avec zoom, seuil et affichage en
    couleurs. L'image de niveau est en CV_32SC1.

    $ make ex01-transfos
    $ ./ex01-transfos [-mag width height] [-thr seuil] image_in [image_out]

    CC BY-SA Edouard.Thiel@univ-amu.fr - 07/09/2020

                        --------------------------------

    Renommez ce fichier tp<n°>-<vos-noms>.cpp 
    Écrivez ci-dessous vos NOMS Prénoms et la date de la version :

    DOITEAU Laurent - version du 03/10/20
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <opencv2/opencv.hpp>
#include "gd-util.hpp"


//----------------------------------- M Y -------------------------------------

class My {
  public:
    cv::Mat img_src, img_res1, img_res2, img_niv, img_coul;
    Loupe loupe;
    int seuil = 127;
    int clic_x = 0;
    int clic_y = 0;
    int clic_n = 0;

    enum Recalc { R_RIEN, R_LOUPE, R_TRANSFOS, R_SEUIL };
    Recalc recalc = R_SEUIL;

    void reset_recalc ()             { recalc = R_RIEN; }
    void set_recalc   (Recalc level) { if (level > recalc) recalc = level; }
    int  need_recalc  (Recalc level) { return level <= recalc; }

    // Rajoutez ici des codes A_TRANSx pour le calcul et l'affichage
    enum Affi { A_ORIG, A_SEUIL, A_TRANS1, A_TRANS2, A_TRANS3, A_TRANS4};
    Affi affi = A_ORIG;
};


//----------------------- T R A N S F O R M A T I O N S -----------------------

// Placez ici vos fonctions de transformations à la place de ces exemples

struct Pvoisins{

    int p = 0;

    int hg = 0;
    int h = 0;
    int hd = 0;

    int bg = 0;
    int b = 0;
    int bd = 0;

    int g = 0;
    int d = 0;

    Pvoisins& operator=(const Pvoisins& P)
    {
        p = P.p;

        hg = P.hg;
        h = P.h;
        hd = P.hd;

        bg = P.bg;
        b = P.b;
        bd = P.bd;

        g = P.g;
        d = P.d;

        return *this;
    }


};

Pvoisins return_voisins(const cv::Mat& img_niv, int x, int y)
{
    Pvoisins Presult;

    Presult.p = img_niv.at<int>(y,x);

    if(y > 0)
    {
        Presult.h = img_niv.at<int>(y-1,x);

        if(x > 0)
            Presult.hg = img_niv.at<int>(y-1,x-1);

        if(x < img_niv.cols-1)
            Presult.hd = img_niv.at<int>(y-1,x+1);
    }


    if(y < img_niv.rows-1)
    {
        Presult.b = img_niv.at<int>(y+1,x);

        if(x > 0)
            Presult.bg = img_niv.at<int>(y+1,x-1);

        if(x < img_niv.cols-1)
            Presult.bd = img_niv.at<int>(y+1,x+1);
    }

    if(x > 0)
        Presult.g = img_niv.at<int>(y,x-1);

    if(x < img_niv.cols-1)
        Presult.d = img_niv.at<int>(y,x+1);

    return Presult;
}


void marquer_contours_c8 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    cv::Mat img_copy;
    img_niv.copyTo(img_copy);

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {

        Pvoisins pResult = return_voisins(img_niv, x, y);

        if(pResult.p == 255)
        {
            if(pResult.h == 0 || pResult.b == 0  || pResult.g == 0 || pResult.d == 0)
                 img_copy.at<int>(y,x) = 1;
            else
                img_copy.at<int>(y,x) = 255;
        }
    }

    img_copy.copyTo(img_niv);
}

void marquer_contours_c4 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    cv::Mat img_copy;
    img_niv.copyTo(img_copy);

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {

        Pvoisins pResult = return_voisins(img_niv, x, y);

        if(pResult.p == 255)
        {
            if(pResult.hg == 0 || pResult.h == 0 || pResult.hd == 0 || pResult.bg == 0 || pResult.b == 0 || pResult.bd == 0 || pResult.g == 0 || pResult.d == 0)
                 img_copy.at<int>(y,x) = 1;
            else
                img_copy.at<int>(y,x) = 255;
        }
    }

    img_copy.copyTo(img_niv);
}

#include <vector>
#include <map>

std::vector<int> label_detect(cv::Mat labels, int x, int y)
{
    std::vector<int> result;

    if(y > 0)
    {
        int h = labels.at<int>(y-1,x);
        if(h != 0 && h != 255)
            result.push_back(h);

        if(x > 0)
        {
            int hg = labels.at<int>(y-1,x-1);
            if(hg != 0 && hg != 255)
                result.push_back(hg);
        }

        if(x < labels.cols-1)
        {
            int hd = labels.at<int>(y-1,x+1);
            if(hd != 0 && hd != 255)
                result.push_back(hd);
        }
    }


    if(y < labels.rows-1)
    {
        int b = labels.at<int>(y+1,x);
        if(b != 0 && b != 255)
            result.push_back(b);

        if(x > 0)
        {
            int bg = labels.at<int>(y+1,x-1);
            if(bg != 0 && bg != 255)
                result.push_back(bg);
        }

        if(x < labels.cols-1)
        {
            int bd = labels.at<int>(y+1,x+1);
            if(bd != 0 && bd != 255)
                result.push_back(bd);
        }
    }

    if(x > 0)
    {
        int g = labels.at<int>(y,x-1);
        if(g != 0 && g != 255)
            result.push_back(g);
    }

    if(x < labels.cols-1)
    {
        int d = labels.at<int>(y,x+1);
        if(d != 0 && d != 255)
            result.push_back(d);  
    }
    
    return result;
}

void numeroter_contours_c8 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    cv::Mat labels;
    img_niv.copyTo(labels);
        
    int label = 1;
    int label_final = 1;
    std::map<int, int> labels_equi;

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        Pvoisins pResult = return_voisins(img_niv, x, y);

        if(pResult.p == 255)
        {
            if(pResult.h == 0 || pResult.b == 0 || pResult.g == 0 || pResult.d == 0)
            {
                std::vector<int> result = label_detect(labels, x, y);
                
                if(result.empty())
                {
                    labels.at<int>(y, x) = label;
                    label++;
                    if(label == 255)
                        label = 256;
                }
                else if(result.size() == 1)
                {
                    labels.at<int>(y, x) = result.front();
                }
                else
                {
                    labels.at<int>(y, x) = result.front();

                    for(int nb : result)
                    {
                        if (labels_equi.count(nb)==0)
                        {
                            labels_equi.insert(std::pair<int, int>(nb, label_final));
                        }
                        else
                        {
                            int label_change = labels_equi[nb];
                            for (auto it = labels_equi.begin(); it != labels_equi.end(); ++it)
                                if (it->second == label_change)
                                    labels_equi[it->first] = label_final;
                        }
                    }
                    label_final++;
                    if(label_final == 255)
                        label_final = 256;
                }
            }
        }


    }

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int p = labels.at<int>(y, x);
        if(p != 0 && p !=255)
            if (labels_equi.count(p)>0)
                labels.at<int>(y, x) = labels_equi[p];
    }
    
    labels.copyTo(img_niv);
}

struct resultN8
{
    int x,y;
    int color;
};


resultN8 N8(cv::Mat img_niv, int x, int y, int dir)
{
    resultN8 result;
    result.x = -1; result.y = -1; result.color = -1; 
    switch (dir)
    {
        case 0:
            result.x = x+1; result.y = y; result.color = img_niv.at<int>(y,x+1); 
            return result;
            break;
        case 1:
            result.x = x+1; result.y = y+1; result.color = img_niv.at<int>(y+1,x+1); 
            return result;
            break;
        case 2:
            result.x = x; result.y = y+1; result.color = img_niv.at<int>(y+1,x); 
            return result;
            break;
        case 3:
            result.x = x-1; result.y = y+1; result.color = img_niv.at<int>(y+1,x-1); 
            return result;
            break;
        case 4:
            result.x = x-1; result.y = y; result.color = img_niv.at<int>(y,x-1); 
            return result;
            break;
        case 5:
            result.x = x-1; result.y = y-1; result.color = img_niv.at<int>(y-1,x-1); 
            return result;
            break;
        case 6:
            result.x = x; result.y = y-1; result.color = img_niv.at<int>(y-1,x); 
            return result;
            break;
        case 7:
            result.x = x+1; result.y = y-1; result.color = img_niv.at<int>(y-1,x+1); 
            return result;
            break; 
        default:
            break;
    }
    return result;
}

std::vector<int> freeman;

bool inPicture(cv::Mat img_niv, int x, int y)
{
    if(x > img_niv.cols-1 || x < 0)
        return false;

    if(y > img_niv.rows-1 || y < 0)
        return false;

    return true;
}

void suivre_un_contour_c8(cv::Mat img_niv, int xA, int yA, int dirA, int num_contour)
{
    int dir_final = 0;

    for (size_t i = 0; i <= 7; i++)
    {
        int d = (dirA+i)%8;
        resultN8 Q = N8(img_niv, xA, yA, d);
        if(Q.color > 0 && inPicture(img_niv, Q.x, Q.y))
        {
            dir_final = (d+4)%8;
            break;
        }
    }

    int x = xA; 
    int y = yA; 
    int dir = dir_final;
    do
    {
        img_niv.at<int>(y,x) = num_contour;
        dir = (dir+4-1) % 8;
        
        size_t i;
        for (i = 0; i <= 7; i++)
        {
            int d = (dir+8-i) % 8;
            resultN8 Q = N8(img_niv, x, y, d);
            if(Q.color > 0 && inPicture(img_niv, Q.x, Q.y))
            {
                x = Q.x;
                y = Q.y;
                dir = d;
                freeman.push_back(d);
                break;
            }    
        }

        if( i > 7)
            return;  
        
    } while ( !(x == xA && y == yA && dir == dir_final) );
}

void afficher_freeman()
{
    std::cout << "s = [ ";
    for(auto i : freeman)
    {
        std::cout << i << ", ";
    }
    std::cout << "]" << std::endl;
}

void effectuer_suivi_contours_c8(cv::Mat img_niv)
{
    int num_contour = 1;
    freeman.clear();

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int p = img_niv.at<int>(y,x);

        if(p == 255)
        {
            int dir = -1;

            if(x == img_niv.cols-1 || img_niv.at<int>(y,x+1) == 0)
                dir = 0;
            else if(y == img_niv.rows-1 || img_niv.at<int>(y+1,x) == 0)
                dir = 2;
            else if(x == 0 || img_niv.at<int>(y,x-1) == 0)
                dir = 4;
            else if(y == 0 || img_niv.at<int>(y-1,x) == 0)
                dir = 6;
            
            if(dir >= 0)
            {
                suivre_un_contour_c8(img_niv, x, y, dir, num_contour++);
                afficher_freeman();
                freeman.clear();
                if(num_contour == 255)
                    num_contour++;
            }
        }
    }
}


// Appelez ici vos transformations selon affi
void effectuer_transformations (My::Affi affi, cv::Mat img_niv)
{
    switch (affi) {
        case My::A_TRANS1 :
            marquer_contours_c8 (img_niv);
            break;
        case My::A_TRANS2 :
            marquer_contours_c4 (img_niv);
            break;
        case My::A_TRANS3 :
            numeroter_contours_c8 (img_niv);
            break;
        case My::A_TRANS4 :
            effectuer_suivi_contours_c8 (img_niv);
            break;
        default : ;
    }
}


//---------------------------- C A L L B A C K S ------------------------------

// Callback des sliders
void onZoomSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->loupe.reborner (my->img_res1, my->img_res2);
    my->set_recalc(My::R_LOUPE);
}

void onSeuilSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->set_recalc(My::R_SEUIL);
}


// Callback pour la souris
void onMouseEventSrc (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->clic_x = x;
            my->clic_y = y;
            my->clic_n = 1;
            break;
        case cv::EVENT_MOUSEMOVE :
            // std::cout << "mouse move " << x << "," << y << std::endl;
            if (my->clic_n == 1) {
                my->loupe.deplacer (my->img_res1, my->img_res2, 
                    x - my->clic_x, y - my->clic_y);
                my->clic_x = x;
                my->clic_y = y;
                my->set_recalc(My::R_LOUPE);
            }
            break;
        case cv::EVENT_LBUTTONUP :
            my->clic_n = 0;
            break;
    }
}


void onMouseEventLoupe (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->loupe.afficher_tableau_valeurs (my->img_niv, x, y, 5, 4);
            break;
    }
}


void afficher_aide() {
    // Indiquez les transformations ici
    std::cout <<
        "Touches du clavier:\n"
        "   a    affiche cette aide\n"
        " hHlL   change la taille de la loupe\n"
        "   i    inverse les couleurs de src\n"
        "   o    affiche l'image src originale\n"
        "   s    affiche l'image src seuillée\n"
        "   1    affiche la transformation 1\n"
        "   2    affiche la transformation 2\n"
        "   3    affiche la transformation 3\n"
        "   4    affiche la transformation 4\n"
        "  esc   quitte\n"
    << std::endl;
}

// Callback "maison" pour le clavier
int onKeyPressEvent (int key, void *data)
{
    My *my = (My*) data;

    if (key < 0) return 0;        // aucune touche pressée
    key &= 255;                   // pour comparer avec un char
    if (key == 27) return -1;     // ESC pour quitter

    switch (key) {
        case 'a' :
            afficher_aide();
            break;
        case 'h' :
        case 'H' :
        case 'l' :
        case 'L' : {
            std::cout << "Taille loupe" << std::endl;
            int h = my->img_res2.rows, w = my->img_res2.cols; 
            if      (key == 'h') h = h >=  200+100 ? h-100 :  200;
            else if (key == 'H') h = h <= 2000-100 ? h+100 : 2000;
            else if (key == 'l') w = w >=  200+100 ? w-100 :  200;
            else if (key == 'L') w = w <= 2000-100 ? w+100 : 2000;
            my->img_res2 = cv::Mat(h, w, CV_8UC3);
            my->loupe.reborner(my->img_res1, my->img_res2);
            my->set_recalc(My::R_LOUPE);
          } break;
        case 'i' :
            std::cout << "Couleurs inversées" << std::endl;
            inverser_couleurs(my->img_src);
            my->set_recalc(My::R_SEUIL);
            break;
        case 'o' :
            std::cout << "Image originale" << std::endl;
            my->affi = My::A_ORIG;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case 's' :
            std::cout << "Image seuillée" << std::endl;
            my->affi = My::A_SEUIL;
            my->set_recalc(My::R_SEUIL);
            break;

        // Rajoutez ici des touches pour les transformations.
        // Dans my->set_recalc, passez :
        //   My::R_SEUIL pour faire le calcul à partir de l'image originale seuillée
        //   My::R_TRANSFOS pour faire le calcul à partir de l'image actuelle
        case '1' :
            std::cout << "Transformation 1" << std::endl;
            my->affi = My::A_TRANS1;
            my->set_recalc(My::R_SEUIL);
            break;
        case '2' :
            std::cout << "Transformation 2" << std::endl;
            my->affi = My::A_TRANS2;
            my->set_recalc(My::R_SEUIL);
            break;
        case '3' :
            std::cout << "Transformation 3" << std::endl;
            my->affi = My::A_TRANS3;
            my->set_recalc(My::R_SEUIL);
            break;
        case '4' :
            std::cout << "Transformation 4" << std::endl;
            my->affi = My::A_TRANS4;
            my->set_recalc(My::R_SEUIL);
            break;

        default :
            //std::cout << "Touche '" << char(key) << "'" << std::endl;
            break;
    }
    return 1;
}


//---------------------------------- M A I N ----------------------------------

void afficher_usage (char *nom_prog) {
    std::cout << "Usage: " << nom_prog
              << "[-mag width height] [-thr seuil] in1 [out2]" 
              << std::endl;
}

int main (int argc, char**argv)
{
    My my;
    char *nom_in1, *nom_out2, *nom_prog = argv[0];
    int zoom_w = 600, zoom_h = 500;

    while (argc-1 > 0) {
        if (!strcmp(argv[1], "-mag")) {
            if (argc-1 < 3) { afficher_usage(nom_prog); return 1; }
            zoom_w = atoi(argv[2]);
            zoom_h = atoi(argv[3]);
            argc -= 3; argv += 3;
        } else if (!strcmp(argv[1], "-thr")) {
            if (argc-1 < 2) { afficher_usage(nom_prog); return 1; }
            my.seuil = atoi(argv[2]);
            argc -= 2; argv += 2;
        } else break;
    }
    if (argc-1 < 1 or argc-1 > 2) { afficher_usage(nom_prog); return 1; }
    nom_in1  = argv[1];
    nom_out2 = (argc-1 == 2) ? argv[2] : NULL;

    // Lecture image
    my.img_src = cv::imread (nom_in1, cv::IMREAD_COLOR);  // produit du 8UC3
    if (my.img_src.empty()) {
        std::cout << "Erreur de lecture" << std::endl;
        return 1;
    }

    // Création résultats
    my.img_res1 = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.img_res2 = cv::Mat(zoom_h, zoom_w, CV_8UC3);
    my.img_niv  = cv::Mat(my.img_src.rows, my.img_src.cols, CV_32SC1);
    my.img_coul = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.loupe.reborner(my.img_res1, my.img_res2);

    // Création fenêtre
    cv::namedWindow ("ImageSrc", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar ("Zoom", "ImageSrc", &my.loupe.zoom, my.loupe.zoom_max, 
        onZoomSlide, &my);
    cv::createTrackbar ("Seuil", "ImageSrc", &my.seuil, 255, 
        onSeuilSlide, &my);
    cv::setMouseCallback ("ImageSrc", onMouseEventSrc, &my);

    cv::namedWindow ("Loupe", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback ("Loupe", onMouseEventLoupe, &my);

    afficher_aide();

    // Boucle d'événements
    for (;;) {

        if (my.need_recalc(My::R_SEUIL)) 
        {
            // std::cout << "Calcul seuil" << std::endl;
            cv::Mat img_gry;
            cv::cvtColor (my.img_src, img_gry, cv::COLOR_BGR2GRAY);
            cv::threshold (img_gry, img_gry, my.seuil, 255, cv::THRESH_BINARY);
            img_gry.convertTo (my.img_niv, CV_32SC1,1., 0.);
        }

        if (my.need_recalc(My::R_TRANSFOS))
        {
            // std::cout << "Calcul transfos" << std::endl;
            if (my.affi != My::A_ORIG) {
                effectuer_transformations (my.affi, my.img_niv);
                representer_en_couleurs_vga (my.img_niv, my.img_coul);
            } else my.img_coul = my.img_src.clone();
        }

        if (my.need_recalc(My::R_LOUPE)) {
            // std::cout << "Calcul loupe puis affichage" << std::endl;
            my.loupe.dessiner_rect    (my.img_coul, my.img_res1);
            my.loupe.dessiner_portion (my.img_coul, my.img_res2);
            cv::imshow ("ImageSrc", my.img_res1);
            cv::imshow ("Loupe"   , my.img_res2);
        }
        my.reset_recalc();

        // Attente du prochain événement sur toutes les fenêtres, avec un
        // timeout de 15ms pour détecter les changements de flags
        int key = cv::waitKey (15);

        // Gestion des événements clavier avec une callback "maison" que l'on
        // appelle nous-même. Les Callbacks souris et slider sont directement
        // appelées par waitKey lors de l'attente.
        if (onKeyPressEvent (key, &my) < 0) break;
    }

    // Enregistrement résultat
    if (nom_out2) {
        if (! cv::imwrite (nom_out2, my.img_coul))
             std::cout << "Erreur d'enregistrement" << std::endl;
        else std::cout << "Enregistrement effectué" << std::endl;
     }
    return 0;
}

