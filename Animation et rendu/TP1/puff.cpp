#include "puff.h"

#include <QMatrix4x4>

Puff::Puff(QVector3D _position, float _size, QVector3D _speed, float _life, QColor _color):position(_position), size(_size), speed(_speed), life(_life),totalLife(life), color(_color)
{
    init();
}

void Puff::useTexture(QOpenGLTexture* _texture)
{
    texture = _texture;
}

void Puff::addProgram(QOpenGLShaderProgram* _program_puff)
{
    program_puff = _program_puff;
}


void Puff::init()
{
    // Création d'une particule de fumée
    GLfloat vertices_particule[] = {
        -0.5f, 0.5f, 0.0f,
        -0.5f,-0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        -0.5f,-0.5f, 0.0f,
        0.5f,-0.5f, 0.0f,
        0.5f, 0.5f, 0.0f
    };

    GLfloat texCoords_particule[] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    QVector<GLfloat> vertData_particule;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_particule.append(vertices_particule[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_particule.append(texCoords_particule[i*2+j]);
    }

    vbo_puff.create();
    vbo_puff.bind();
    vbo_puff.allocate(vertData_particule.constData(), vertData_particule.count() * int(sizeof(GLfloat)));
}


void Puff::animate(float dt)
{
    position = position + speed * dt;
    life -= dt;
}

void Puff::display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix)
{
    vbo_puff.bind();
    program_puff->bind(); // active le shader program des particules

    QMatrix4x4 modelMatrix;
    modelMatrix.translate(position);
    program_puff->setUniformValue("projectionMatrix", projectionMatrix);
    program_puff->setUniformValue("viewMatrix", viewMatrix);
    program_puff->setUniformValue("modelMatrix", modelMatrix);
    program_puff->setUniformValue("particleSize", 1.0f);
    program_puff->setUniformValue("endTime", totalLife);
    program_puff->setUniformValue("elapsedTime", life);
    program_puff->setUniformValue("color", color);


    program_puff->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    program_puff->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    program_puff->enableAttributeArray("in_position");
    program_puff->enableAttributeArray("in_uv");

    texture->bind();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisable(GL_BLEND);
    texture->release();

    program_puff->disableAttributeArray("in_position");
    program_puff->disableAttributeArray("in_uv");
    program_puff->release();
}

void Puff::destroy()
{
    vbo_puff.destroy();
}

