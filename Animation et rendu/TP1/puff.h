#ifndef PUFF_H
#define PUFF_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>


class Puff
{
public:

    Puff(QVector3D _position, float _size, QVector3D _speed, float _life, QColor _color);

    void init();

    void useTexture(QOpenGLTexture* _texture);
    void addProgram(QOpenGLShaderProgram* _program_puff);

    void animate(float dt);
    void display(const QMatrix4x4& projectionMatrix,const QMatrix4x4& viewMatrix);

    void destroy();

    QVector3D position;
    float size;
    QVector3D speed;
    float life;
    float totalLife;
    QColor color;

    QOpenGLShaderProgram *program_puff;
    QOpenGLBuffer vbo_puff;
    QOpenGLTexture *texture;
};

#endif // PUFF_H
