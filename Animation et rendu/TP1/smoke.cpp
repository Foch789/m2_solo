#include "smoke.h"

Smoke::Smoke()
{}

Smoke::Smoke(QVector3D _position, float _timeInterval, QColor _color): position(_position), color(_color), puffsList(), timeInterval(_timeInterval), elapsedTime(0.)
{}

void Smoke::animate(float dt)
{
    elapsedTime += dt;

    if(elapsedTime >= timeInterval)
    {
        elapsedTime = 0.f;
        Puff puff {position, 1, {0, 1, 0}, 20, color};
        puff.useTexture(texture);
        puff.addProgram(program_puff);
        puffsList.push_back(puff);
    }

    std::list<Puff>::iterator i;
    i = puffsList.begin();
    while( i != puffsList.end() )
    {
        if( i->life <= 0 )
            i = puffsList.erase(i);
        else
        {
            i++;
        }
    }

    for(auto& i : puffsList)
        i.animate(dt);
}

void Smoke::display(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix)
{
    for(auto& i : puffsList)
        i.display(projectionMatrix, viewMatrix);
}

void Smoke::useTexture(QOpenGLTexture* _texture)
{
    texture = _texture;
}

void Smoke::addProgram(QOpenGLShaderProgram* _program_puff)
{
    program_puff = _program_puff;
}

void Smoke::destroy()
{
    for(auto& puff: puffsList)
        puff.~Puff();

    delete texture;
    delete program_puff;
}

