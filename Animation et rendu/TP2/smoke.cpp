#include "smoke.h"



Smoke::Smoke()
{}

Smoke::Smoke(QOpenGLFunctions* ecran, QVector3D _position, float _size, float _timeInterval, QColor _color): position(_position), color(_color), sizePuff(_size), puffsList(), timeInterval(_timeInterval), elapsedTime(0.){
    /*static const GLfloat g_vertex_buffer_data[] = {
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        };
    GLuint billboard_vertex_buffer;
    ecran->glGenBuffers(1, &billboard_vertex_buffer);
    ecran->glBindBuffer(GL_ARRAY_BUFFER, billboard_vertex_buffer);
    ecran->glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    // Le VBO contenant la position et la taille des particules
    GLuint particles_position_buffer;
    ecran->glGenBuffers(1, &particles_position_buffer);
    ecran->glBindBuffer(GL_ARRAY_BUFFER, particles_position_buffer);
    // Initialisé avec un tampon vide (NULL) : il sera mis à jour plus tard, à chaque image.
    ecran->glBufferData(GL_ARRAY_BUFFER, 100 * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

    // Le VBO contenant la couleurs des particules
    GLuint particles_color_buffer;
    ecran->glGenBuffers(1, &particles_color_buffer);
    ecran->glBindBuffer(GL_ARRAY_BUFFER, particles_color_buffer);
    // Initialisé avec un tampon vide (NULL) : il sera mis à jour plus tard, à chaque image.
    ecran->glBufferData(GL_ARRAY_BUFFER, 100 * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);*/
}

void Smoke::animate(float dt)
{
    elapsedTime += dt;

    if(elapsedTime >= timeInterval)
    {
        elapsedTime = 0.f;
        Puff puff {position, sizePuff, {0, 1, 0}, 20, color};
        puff.useTexture(texture);
        puff.addProgram(program_puff);
        puffsList.push_back(puff);
    }

    std::list<Puff>::iterator i;
    i = puffsList.begin();
    while( i != puffsList.end() )
    {
        if( i->life <= 0 )
            i = puffsList.erase(i);
        else
        {
            i++;
        }
    }

    for(auto& i : puffsList)
        i.animate(dt);
}

void Smoke::display(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix)
{
    for(auto& i : puffsList)
        i.display(projectionMatrix, viewMatrix);
}

void Smoke::useTexture(QOpenGLTexture* _texture)
{
    texture = _texture;
}

void Smoke::addProgram(QOpenGLShaderProgram* _program_puff)
{
    program_puff = _program_puff;
}

void Smoke::destroy()
{
    for(auto& puff: puffsList)
        puff.~Puff();

    delete texture;
    delete program_puff;
}

