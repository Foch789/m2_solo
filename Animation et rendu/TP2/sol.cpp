#include "sol.h"

#include <QMatrix4x4>

Sol::Sol(){

}

Sol::Sol(QVector3D _position, float _size, QColor _color):position(_position), size(_size), color(_color){
    init();
}

void Sol::useTexture(QOpenGLTexture* _texture){
    texture = _texture;
}

void Sol::addProgram(QOpenGLShaderProgram* _program_sol){
    program_sol = _program_sol;
}


void Sol::init(){
    // Création du sol
    float tailleSol = 20.0f;

    GLfloat vertices_sol[] = {
        -tailleSol, 0.0f,-tailleSol,
        -tailleSol, 0.0f, tailleSol,
        tailleSol, 0.0f, tailleSol,
        tailleSol, 0.0f, tailleSol,
        tailleSol, 0.0f,-tailleSol,
        -tailleSol, 0.0f,-tailleSol
    };

    GLfloat texCoords_sol[] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f
    };

    QVector<GLfloat> vertData_sol;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_sol.append(vertices_sol[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_sol.append(texCoords_sol[i*2+j]);
    }

    vbo_sol.create();
    vbo_sol.bind();
    vbo_sol.allocate(vertData_sol.constData(), vertData_sol.count() * int(sizeof(GLfloat)));
}


void Sol::display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix){
    vbo_sol.bind();
    program_sol->bind(); // active le shader program du sol

    QMatrix4x4 modelMatrixSol;
    modelMatrixSol.translate(0.0f, 0.0f, 0.0f);
    program_sol->setUniformValue("projectionMatrix", projectionMatrix);
    program_sol->setUniformValue("viewMatrix", viewMatrix);
    program_sol->setUniformValue("modelMatrix", modelMatrixSol);

    program_sol->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    program_sol->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    program_sol->enableAttributeArray("in_position");
    program_sol->enableAttributeArray("in_uv");

    texture->bind();
    glDrawArrays(GL_TRIANGLES, 0, 6);
    texture->release();

    program_sol->disableAttributeArray("in_position");
    program_sol->disableAttributeArray("in_uv");
    program_sol->release();
}

void Sol::destroy(){
    vbo_sol.destroy();
}

