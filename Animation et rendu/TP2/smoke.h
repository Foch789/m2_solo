#ifndef SMOKE_H
#define SMOKE_H

#include <QList>
#include <QVector3D>

#include <QOpenGLFunctions>

#include "puff.h"

class Smoke
{
public:
    Smoke();
    Smoke(QOpenGLFunctions* ecran, QVector3D _position, float _size, float _timeInterval, QColor _color);

    void animate(float dt);
    void display(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix);

    void useTexture(QOpenGLTexture* _texture);
    void addProgram(QOpenGLShaderProgram* _program_puff);

    void destroy();

    QVector3D position;
    QColor color;
    float sizePuff;
    std::list<Puff> puffsList;
    float timeInterval;
    float elapsedTime;

    QOpenGLShaderProgram *program_puff;
    QOpenGLTexture *texture;
};

#endif // SMOKE_H
