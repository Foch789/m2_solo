#include "poisson.h"

#include "puff.h"

#include <QMatrix4x4>

Poisson::Poisson(QVector3D _position, float _size, QVector3D _speed, float _distance, QColor _color):position(_position), size(_size), speed(_speed), distance(_distance), color(_color) {
    init();
}

void Poisson::useTexture(QOpenGLTexture* _texture) {
    texture = _texture;
}

void Poisson::addProgram(QOpenGLShaderProgram* _program_puff) {
    program_poisson = _program_puff;
}

void Poisson::init() {
    // Création d'une particule de fumée
    GLfloat vertices_particule[] = {
        -0.5f, 0.5f, 0.0f,
        -0.5f,-0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        -0.5f,-0.5f, 0.0f,
        0.5f,-0.5f, 0.0f,
        0.5f, 0.5f, 0.0f
    };

    GLfloat texCoords_particule[] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    QVector<GLfloat> vertData_particule;
    for (int i = 0; i < 6; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_particule.append(vertices_particule[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_particule.append(texCoords_particule[i*2+j]);
    }

    vbo_poisson.create();
    vbo_poisson.bind();
    vbo_poisson.allocate(vertData_particule.constData(), vertData_particule.count() * int(sizeof(GLfloat)));
}


bool Poisson::dans_voisinage(QVector3D p) {

}


void Poisson::animate(float dt) {
    position = position + speed * dt;
}

void Poisson::display(const QMatrix4x4& projectionMatrix, const QMatrix4x4& viewMatrix) {
    vbo_poisson.bind();
    program_poisson->bind(); // active le shader program des particules

    QMatrix4x4 modelMatrix;
    modelMatrix.translate(position);
    program_poisson->setUniformValue("projectionMatrix", projectionMatrix);
    program_poisson->setUniformValue("viewMatrix", viewMatrix);
    program_poisson->setUniformValue("modelMatrix", modelMatrix);
    program_poisson->setUniformValue("particleSize", 1.0f);
    program_poisson->setUniformValue("size", size);
    program_poisson->setUniformValue("color", color);

    program_poisson->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    program_poisson->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    program_poisson->enableAttributeArray("in_position");
    program_poisson->enableAttributeArray("in_uv");

    texture->bind();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisable(GL_BLEND);
    texture->release();

    program_poisson->disableAttributeArray("in_position");
    program_poisson->disableAttributeArray("in_uv");
    program_poisson->release();
}

void Poisson::destroy() {
    vbo_poisson.destroy();
}

