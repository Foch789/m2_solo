#ifndef POISSON_H
#define POISSON_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

class Poisson
{
public:
    Poisson(QVector3D _position, float _size, QVector3D _speed, float _distance, QColor _color);

    void init();

    void useTexture(QOpenGLTexture* _texture);
    void addProgram(QOpenGLShaderProgram* _program_puff);

    void animate(float dt);
    bool dans_voisinage(QVector3D p);
    void display(const QMatrix4x4& projectionMatrix,const QMatrix4x4& viewMatrix);

    void destroy();

    QVector3D position;
    float size;
    QVector3D speed;
    float distance;
    QColor color;

    QOpenGLShaderProgram *program_poisson;
    QOpenGLBuffer vbo_poisson;
    QOpenGLTexture *texture;
};

#endif // POISSON_H
