#ifndef SOL_H
#define SOL_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>


class Sol
{
public:

    Sol();
    Sol(QVector3D _position, float _size, QColor _color);

    void init();

    void useTexture(QOpenGLTexture* _texture);
    void addProgram(QOpenGLShaderProgram* _program_sol);

    void display(const QMatrix4x4& projectionMatrix,const QMatrix4x4& viewMatrix);

    void destroy();

    QVector3D position;
    float size;
    QColor color;

    QOpenGLShaderProgram *program_sol;
    QOpenGLBuffer vbo_sol;
    QOpenGLTexture *texture;
};


#endif // SOL_H
