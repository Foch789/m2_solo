varying vec4 uv;
uniform sampler2D texture;

uniform float endTime;
uniform float elapsedTime;
uniform vec4 color;

void main() {
    float factor = ((endTime - elapsedTime)/endTime) * 16.0;

    float col = floor(mod(factor, 4.0));
    float row = floor(factor / 4.0);

    vec4 text = texture2D(texture, vec2(((uv.s + col)/4.0), ((uv.t + row)/4.0)));
    gl_FragColor = text * vec4(1.0, 1.0, 1.0, elapsedTime/endTime) * color;
}
