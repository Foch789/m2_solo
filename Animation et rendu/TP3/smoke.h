#ifndef SMOKE_H
#define SMOKE_H

#include "puff.h"
#include <QList>

class Smoke
{
public:
    Smoke();
    Smoke(QVector3D _position, float _timeInterval, QColor _color);
    //Smoke(QVector3D _position, Puff _puff, float _timeInterval);

    void animate(float dt);
    void display(const QMatrix4x4 &projectionMatrix, const QMatrix4x4 &viewMatrix);

    void useTexture(QOpenGLTexture* _texture);
    void addProgram(QOpenGLShaderProgram* _program_puff);

    void destroy();

    QVector3D position;
    QColor color;
    std::list<Puff> puffsList;
    float timeInterval;
    float elapsedTime;

    QOpenGLShaderProgram *program_puff;
    QOpenGLTexture *texture;
};

#endif // SMOKE_H
