#include "mesh.h"
#include "algo.h"
#include <iostream>

bool MyMesh::read(const std::string &filename)
{
    std::clog << "Lecture de " << filename << std::endl;

    using OpenMesh::IO::Options;
    Options opt;
    opt += Options::FaceNormal;
    opt += Options::VertexNormal;
    if (!OpenMesh::IO::read_mesh(*this, filename, opt)) {
      return false;
    }

    if (!opt.face_has_normal()) {
      std::clog << "Calcul des normales aux faces." << std::endl;
      update_face_normals();
    }

    if (!opt.vertex_has_normal()) {
      std::clog << "Calcul des normales aux sommets." << std::endl;
      update_vertex_normals();
    }

    return true;
}

void MyMesh::update_custom_properties()
{
    update_face_area();
}

void MyMesh::face_vertices(FaceHandle face, std::array<MyMesh::Point, 3>& vertices) const
{
    auto fv_it = cfv_ccwiter(face);
    for (size_t i = 0; i < 3; ++i) {
      assert(fv_it.is_valid());
      vertices[i] = point(*fv_it);
      ++fv_it;
    }
}

float MyMesh::calc_face_area(FaceHandle face) const
{
    std::array<MyMesh::Point, 3> vertices;
    face_vertices(face, vertices);

    return cross(vertices[0] - vertices[1], vertices[2] - vertices[1]).norm() / 2.f;
}

void MyMesh::update_face_area()
{
    for (auto f_it = faces_begin(); f_it != faces_end(); f_it++) {
        data(*f_it).area = calc_face_area(*f_it);
    }
}

MyMesh::Point MyMesh::calc_barycenter() const {
    Point cog;
    cog[0] = cog[1] = cog[2] = 0.0;
    for(auto v_it = vertices_begin(); v_it != vertices_end(); v_it++) {
        cog += point(*v_it);
    }
    cog /= n_vertices();
    return cog;
}

MyMesh::SafeBox MyMesh::calc_safe_box() const {
    Point vMax(std::numeric_limits<float>::min());
    Point vMin(std::numeric_limits<float>::max());

    for(auto v_it = vertices_begin(); v_it != vertices_end(); v_it++) {
        const auto& currentPoint = point(*v_it);
        vMax.maximize(currentPoint);
        vMin.minimize(currentPoint);
    }

    SafeBox sb;

    sb.max = vMax;
    sb.min = vMin;

    return sb;
}

float MyMesh::calc_normal_vertex_deviation(VertexHandle vertex) const
{
    return calc_list_stats<float, VertexFaceIter, FaceHandle>(cvf_begin(vertex), cvf_end(vertex), [&](FaceHandle face) {
        return angle(normal(vertex), normal(face));
    }).max;
}
