#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include "mesh.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum TypeText {INFO, ERROR, DEBUG};

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void displayMesh(MyMesh *_mesh, bool isTemperatureMap = false, float mapRange = -1);
    void resetAllColorsAndThickness(MyMesh* _mesh);

    void laplace_beltrami(MyMesh* _mesh);
    void laplace_beltrami_matrix(MyMesh* _mesh);

    void loadMesh(QString fileName);
    void writeConsole(QString txt, TypeText type = TypeText::INFO);

private slots:
    void on_pushButton_chargement_clicked();
    void on_pushButton_laplace_clicked();
    void on_pushButton_laplacematrices_clicked();


    void dragEnterEvent(QDragEnterEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private:

    MyMesh mesh;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
