#ifndef MESH_H
#define MESH_H

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <QVector3D>

using namespace OpenMesh;
using namespace OpenMesh::Attributes;

struct MyTraits : public OpenMesh::DefaultTraits
{
    // use vertex normals and vertex colors
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    // store the previous halfedge
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    // use face normals face colors
    FaceAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    EdgeAttributes( OpenMesh::Attributes::Color | OpenMesh::Attributes::Status );
    // vertex thickness
    VertexTraits{float thickness; float value;};
    // edge thickness
    EdgeTraits{float thickness;};
    // face area
    FaceTraits{float area;}; // TODO: utiliser custom property
};

class MyMesh : public OpenMesh::TriMesh_ArrayKernelT<MyTraits> {
public:
    using TriMesh_ArrayKernelT<MyTraits>::TriMesh_ArrayKernelT;

    bool read(const std::string& filename);

    // les normales doivent être déjà calculées
    void update_custom_properties();

    // stocke dans un tableau en paramètre les 3 points d'une face (faces triangulaires)
    void face_vertices(FaceHandle face, std::array<Point, 3>& vertices) const;
    // calcule la surface d'une face donnée (avec le produit vectoriel)
    float calc_face_area(FaceHandle face) const;
    // met à jour l'aire de chaque face du maillage (dans "data")
    void update_face_area();
    // calcule le barycentre de tous les sommets d'un mesh
    Point calc_barycenter() const;


    // structure pour la boîte englobante
    struct SafeBox {
        Point min;
        Point max;
    };

    // calcule la boite englobante d'un mesh
    SafeBox calc_safe_box() const;

    float calc_normal_vertex_deviation(VertexHandle vertex) const;
};

#endif // MESH_H
