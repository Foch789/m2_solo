#ifndef ALGO_H
#define ALGO_H

#include <cmath>
#include <limits>
#include <functional>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>

#include <OpenMesh/Core/Geometry/VectorT.hh>
using Vector3f = OpenMesh::Vec3f;

// strucure template stockant le min le max et la moyenne d'un élément générique
template<typename Element>
struct ListStats {
    Element min, max, sum;
    size_t count;

    Element mean() const { return sum / count; }
};

// fonction template retournant le min, le MinMaxMean d'un élément générique
template<typename Element, typename Iterator, typename IteratorItem>
ListStats<Element> calc_list_stats(const Iterator &begin, const Iterator &end, const std::function<Element(IteratorItem)>& func) {
    ListStats<Element> r;
    r.min   = std::numeric_limits<Element>::max();
    r.max   = std::numeric_limits<Element>::min();
    r.sum   = 0;
    r.count = 0;

    for (Iterator it = begin; it != end; ++it) {
      Element v = func(*it);

      if (std::isnan(v)) {
        continue;
      }

      r.min = std::min(r.min, v);
      r.max = std::max(r.max, v);
      r.sum = r.sum + v;
      r.count += 1;
    }

    return r;
}

template<typename Element>
struct Freq {
    std::vector<std::string> freq_labels;
    std::vector<size_t> freq_values;

    void as_csv(std::ostream& os) {
        for (size_t i = 0; i < freq_labels.size(); ++i) {
            if (i != 0) os << ',';
            os << freq_labels[i];
        }
        os << std::endl;
        for (size_t i = 0; i < freq_values.size(); ++i) {
            if (i != 0) os << ',';
            os << freq_values[i];
        }
        os << std::endl;
    }
};

template<typename Element, typename Iterator, typename IteratorItem>
Freq<Element> calc_freq(const Iterator &begin, const Iterator &end, const std::function<Element(IteratorItem)>& func, const Element& min, const Element& max, size_t nb_freq) {
    Freq<Element> r;
    assert(min < max);
    r.freq_labels.resize(nb_freq);
    r.freq_values.resize(nb_freq, 0);

    for (size_t i = 0; i < nb_freq; ++i) {
        std::stringstream ss;
        ss << '[' << int(float(i)/nb_freq*100.f) << "% - " << int(float(i+1)/nb_freq*100.f) << '%' << ((i == nb_freq - 1) ? ']' : '[')
           << " (" << (min + float(i)/nb_freq*(max-min)) << " - " << (min + float(i+1)/nb_freq*(max-min)) << ')';
        r.freq_labels[i] = ss.str();
    }

    for (Iterator it = begin; it != end; ++it) {
        Element v = func(*it);

        if (std::isnan(v)) {
          continue;
        }

        int freq = std::min(0.9f, (v - min) / (max - min)) * nb_freq;
        r.freq_values[freq] += 1;
    }

    return r;
}

template<typename Element, typename Iterator, typename IteratorItem>
Freq<Element> calc_freq(const Iterator &begin, const Iterator &end, const std::function<Element(IteratorItem)>& func) {
    std::map<Element, size_t> freq;
    for (Iterator it = begin; it != end; ++it) {
        Element v = func(*it);

        if (std::isnan(v)) {
          continue;
        }

        auto count = freq.find(v);
        if (count != freq.end()) {
            count->second += 1;
        } else {
            freq[v] = 1;
        }
    }

    Freq<Element> r;
    r.freq_labels.reserve(freq.size());
    r.freq_values.reserve(freq.size());

    for (const auto& kv : freq) {
        std::stringstream ss;
        ss << kv.first;
        r.freq_labels.push_back(ss.str());
        r.freq_values.push_back(kv.second);
    }

    return r;
}

float clamp(float v, float lo, float hi);

// retourne acos sur une valeur qui est recoupé entre [-1 , 1]
float clampedAcos(float value);

// retourne un angle entre [0 , π]
float angle(const Vector3f& v1, const Vector3f& v2);

// normal doit être normalisé
// retourne un angle entre [-π , +π]
float signedAngle(const Vector3f& v1, const Vector3f& v2, const Vector3f& normal);

float cot(float angle);

#endif // ALGO_H
