#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "algo.h"

#include <iostream>
#include <QMimeData>
#include <QScrollBar>
#include <QGenericMatrix>
#include <QMap>
#include <Eigen/Dense>

/* **** début de la partie boutons et IHM **** */

// exemple pour charger un fichier .obj
void MainWindow::on_pushButton_chargement_clicked()
{
    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));
    loadMesh(fileName);  
}

void MainWindow::on_pushButton_laplace_clicked()
{
    laplace_beltrami(&mesh);
}

void MainWindow::on_pushButton_laplacematrices_clicked()
{
    laplace_beltrami_matrix(&mesh);
}

/* **** fin de la partie boutons et IHM **** */

void MainWindow::laplace_beltrami(MyMesh* _mesh)
{
    auto mesh = _mesh;

    auto area = [&](FaceHandle face) { return mesh->data(face).area; };

    for(auto it_v = mesh->vertices_begin(); it_v != mesh->vertices_end(); ++it_v)
    {
        float areaSum = 0.0;
        for(auto it_vf = mesh->vf_iter(*it_v); it_vf.is_valid(); ++it_vf)
            areaSum += area(*it_vf);
        float barycentrique_area = areaSum/3.;

        QVector<VertexHandle> verticesNeighbour;
        for(auto it_vv = mesh->vv_iter(*it_v); it_vv.is_valid(); ++it_vv)
            verticesNeighbour.push_back(*it_vv);
        auto sizeVertices = verticesNeighbour.size();

        Vector3f ptSum {0, 0, 0};
        for(int i = 0; i < verticesNeighbour.size(); ++i)
        {
            auto next = verticesNeighbour[(i+1)%sizeVertices];
            auto now = verticesNeighbour[i];
            auto past = verticesNeighbour[(sizeVertices + i-1)%sizeVertices];

            Vector3f Av1 = mesh->point(next) - mesh->point(*it_v);
            Vector3f Av2 = mesh->point(next) - mesh->point(now);
            float alpha = cot(angle(Av1, Av2));

            Vector3f Bv1 = mesh->point(past) - mesh->point(*it_v);
            Vector3f Bv2 = mesh->point(past) - mesh->point(now);
            float beta = cot(angle(Bv1, Bv2));

            Vector3f pt = mesh->point(now) - mesh->point(*it_v);

            ptSum += (alpha + beta) * pt;
        }

        Vector3f result =  mesh->point(*it_v) + (ui->box_h->value() * ui->box_lamda->value()) * (ptSum/(barycentrique_area * 2.));
        mesh->set_point(*it_v, result);
    }

    // on affiche le maillage
    displayMesh(mesh);
}


void MainWindow::laplace_beltrami_matrix(MyMesh* _mesh){

    auto mesh = _mesh;

    int nvertex = mesh->n_vertices();

    auto area = [&](FaceHandle face) { return mesh->data(face).area; };

    Eigen::MatrixXd M(nvertex, nvertex);
    M.setZero();

    Eigen::MatrixXd D(nvertex, nvertex);
    D.setZero();

    Eigen::MatrixXd V(nvertex, 3);

    for(auto vi = mesh->vertices_begin(); vi != mesh->vertices_end(); ++vi){

        V(vi->idx(), 0) = mesh->point(*vi)[0];
        V(vi->idx(), 1) = mesh->point(*vi)[1];
        V(vi->idx(), 2) = mesh->point(*vi)[2];

        float areaSum = 0.0;
        for(auto it_vf = mesh->vf_iter(*vi); it_vf.is_valid(); ++it_vf)
            areaSum += area(*it_vf);
        float barycentrique_area = areaSum/3.;
        D(vi->idx(), vi->idx()) = 1./(2.*barycentrique_area);

        QVector<VertexHandle> verticesNeighbour;
        for(auto it_vv = mesh->vv_iter(*vi); it_vv.is_valid(); ++it_vv)
            verticesNeighbour.push_back(*it_vv);
        auto sizeVertices = verticesNeighbour.size();

        float sum = 0.;
        for(int i = 0; i < verticesNeighbour.size(); ++i){
            auto next = verticesNeighbour[(i+1)%sizeVertices];
            auto now = verticesNeighbour[i];
            auto past = verticesNeighbour[(sizeVertices + i-1)%sizeVertices];

            Vector3f Av1 = mesh->point(next) - mesh->point(*vi);
            Vector3f Av2 = mesh->point(next) - mesh->point(now);
            float alpha = cot(angle(Av1, Av2));

            Vector3f Bv1 = mesh->point(past) - mesh->point(*vi);
            Vector3f Bv2 = mesh->point(past) - mesh->point(now);
            float beta = cot(angle(Bv1, Bv2));

            M(vi->idx(), now.idx()) = (alpha+beta);

            sum -= (alpha+beta);
        }
        M(vi->idx(), vi->idx()) = sum;
    }

    V = M*D*V;
    V *= (ui->box_h->value() * ui->box_lamda->value());

    for(auto vi = mesh->vertices_begin(); vi != mesh->vertices_end(); ++vi){
        Vector3f p{V(vi->idx(), 0), V(vi->idx(), 1), V(vi->idx(), 2)};
        Vector3f result = mesh->point(*vi) + p;
        mesh->set_point(*vi, result);
    }

    displayMesh(mesh);
}

/* **** fonctions supplémentaires **** */
// permet d'initialiser les couleurs et les épaisseurs des élements du maillage
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 1;
        _mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        _mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 1;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

// charge un objet MyMesh dans l'environnement OpenGL
void MainWindow::displayMesh(MyMesh* _mesh, bool isTemperatureMap, float mapRange)
{
    GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
    GLfloat* triCols = new GLfloat[_mesh->n_faces() * 3 * 3];
    GLfloat* triVerts = new GLfloat[_mesh->n_faces() * 3 * 3];

    int i = 0;

    if(isTemperatureMap)
    {
        QVector<float> values;

        if(mapRange == -1)
        {
            for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
                values.append(fabs(_mesh->data(*curVert).value));
            qSort(values);
            mapRange = values.at(values.size()*0.8);
            qDebug() << "mapRange" << mapRange;
        }

        float range = mapRange;
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }
    else
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, _mesh->n_faces() * 3 * 3, triIndiceArray, _mesh->n_faces() * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        if(t > 0)
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;

    auto cog = _mesh->calc_barycenter();
    auto box = _mesh->calc_safe_box();
    ui->displayWidget->set_scene_pos(cog, std::max((cog - box.min).length(), (cog - box.max).length()));
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setAcceptDrops(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadMesh(QString fileName)
{
    // chargement du fichier .obj dans la variable globale "mesh"
    if(!mesh.read(fileName.toStdString()))
    {
        std::cerr << "Erreur : Le chargement du mesh " <<  fileName.toUtf8().constData() << " a echoué." << std::endl;
        writeConsole("Erreur : Le chargement du mesh " + QString(fileName.toUtf8().constData()) + " a echoué.", TypeText::ERROR);
    }

    if(!mesh.is_trimesh()) // TODO: il me semble que c n'est jamais le cas comme c un trimesh
    {
        std::cerr << "Erreur : Le chargement du mesh " <<  fileName.toUtf8().constData() << " a echoué." << std::endl;
        std::cerr << "Erreur : Le mesh n'est pas triangulé." << std::endl;
        writeConsole("Erreur : Le chargement du mesh " + QString(fileName.toUtf8().constData()) + " a echoué.", TypeText::ERROR);
        writeConsole("Erreur : Le mesh n'est pas triangulé.", TypeText::ERROR);
        return;
    }

    for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it)
    {
        int compteur = 0;
        for(MyMesh::FaceFaceIter ff_it = mesh.ff_iter(*f_it); ff_it.is_valid(); ++ff_it)
            compteur++;

        if(compteur==0)
        {
            std::cerr << "Erreur : Le chargement du mesh " <<  fileName.toUtf8().constData() << " a echoué." << std::endl;
            std::cerr << "Erreur : La face " << *f_it << " est isolée." << std::endl;
            writeConsole("Erreur : Le chargement du mesh " + QString(fileName.toUtf8().constData()) + " a echoué.", TypeText::ERROR);
            writeConsole("Erreur : La face " + QString::number(f_it->idx()) + " est isolée.", TypeText::ERROR);
            return;
        }
    }

    for (MyMesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it)
    {
        int compteur = 0;
        for(MyMesh::VertexEdgeIter ve_it = mesh.ve_iter(*v_it); ve_it.is_valid(); ++ve_it)
            compteur++;

        if(compteur == 0)
        {
            std::cerr << "Erreur : Le chargement du mesh " <<  fileName.toUtf8().constData() << " a echoué." << std::endl;
            std::cerr << "Erreur : Le point 3D " << *v_it << " est isolé." << std::endl;
            writeConsole("Erreur : Le chargement du mesh " + QString(fileName.toUtf8().constData()) + " a echoué.", TypeText::ERROR);
            writeConsole("Erreur : Le point 3D " + QString::number(v_it->idx()) + " est isolé.", TypeText::ERROR);
            return;
        }
    }

    for (MyMesh::EdgeIter e_it = mesh.edges_begin(); e_it!=mesh.edges_end(); ++e_it)
    {
        if(!mesh.face_handle(mesh.halfedge_handle(*e_it, 0)).is_valid() && !mesh.face_handle(mesh.halfedge_handle(*e_it, 1)).is_valid())
        {
            std::cerr << "Erreur : Le chargement du mesh " <<  fileName.toUtf8().constData() << " a echoué." << std::endl;
            std::cerr << "Erreur : L'arête " << *e_it << " est isolée." << std::endl;
            writeConsole("Erreur : Le chargement du mesh " + QString(fileName.toUtf8().constData()) + " a echoué.", TypeText::ERROR);
            writeConsole("Erreur : L'arête " + QString::number(e_it->idx()) + " est isolé.", TypeText::ERROR);
            return;
        }
    }

    mesh.update_custom_properties();

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
}

void MainWindow::writeConsole(QString txt, TypeText type)
{
    switch (type)
    {
    case TypeText::INFO:
        ui->Console->appendPlainText(txt);
        break;
    case TypeText::ERROR:
        ui->Console->appendHtml("<p style=\"color:red;white-space:pre\">" + txt + "</p>");
        break;
    case TypeText::DEBUG:
        ui->Console->appendPlainText(txt);
        break;
    }
    ui->Console->verticalScrollBar()->setValue(ui->Console->verticalScrollBar()->maximum());
}

/* **** Event **** */
void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();

}

void MainWindow::dropEvent(QDropEvent* event)
{
    const QMimeData* file = event->mimeData();
    if (file->hasUrls()) {
        if (file->urls().isEmpty()) {
            return;
        }
        for (auto i : file->urls()) {
            QString fileName = i.toLocalFile();
            qDebug() << i.toLocalFile().toLocal8Bit().constData();
            loadMesh(fileName);
            event->acceptProposedAction();
        }
    }
}

